/* 
Viết chương trình có một ô input, một button.
Khi click vào button thì in ra các số nguyên tố từ 1 tới giá trị của ô input.
*/

function printPrimeNumber() {
  // Lấy giá trị n từ người dùng
  let n = document.querySelector("#js_ex1__num-n").value * 1;
  // Cho biến primeList là một chuỗi rỗng
  let primeList = "";
  // Loop từ giá trị 2 tới giá trị n
  for (let i = 2; i <= n; i++) {
    // Cho biến divisor (ước của số i) bằng 0
    let divisor = 0;
    // Chạy loop từ số (i - 1) tới số 2
    for (let j = i - 1; j > 1; j--) {
      // Nếu số i chia hết cho số j thì tăng divisor lên 1, nếu không thì không làm gì cả và tiếp tục chạy loop
      if (i % j === 0) {
        divisor++;
      }
    }
    // Sau khi chạy xong loop thì nếu số i không có thêm ước số thì cho vào primeList, nếu không thì tiếp tục chạy loop
    if (divisor === 0) {
      primeList += i + " ";
    }
  }
  // In dãy số nguyên cho người dùng
  document.querySelector("#js_ex1__result").textContent = primeList;
}
